# MLX90363 RTIC application
An example project for the [mlx90363 device driver crate](https://gitlab.com/jspngh/mlx90363-rs)
using the [RTIC framework](https://github.com/rtic-rs/cortex-m-rtic) and the [nRF52840 HAL](https://github.com/nrf-rs/nrf-hal).

## License
Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution
Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the
work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
