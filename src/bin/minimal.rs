#![no_main]
#![no_std]

use mlx90363_rtic as _;

#[rtic::app(
    device = nrf52840_hal::pac,
    peripherals = true,
    dispatchers = [SWI0_EGU0]
)]
mod app {
    use nrf52840_hal as hal;

    use embedded_hal::timer::CountDown;
    use fugit::{self, ExtU32};
    use hal::gpio::{self, Output, PushPull};
    use hal::spim::{self, Spim};
    use hal::timer::Timer;
    use heapless::spsc::{Consumer, Producer, Queue};
    use mlx90363::{opcode, Mlx90363};
    use mlx90363_rtic::monotimer::MonoTimer;

    #[monotonic(binds = TIMER1, default = true)]
    type Tonic = MonoTimer<nrf52840_hal::pac::TIMER1>;

    type Mlx90363Instance = Mlx90363<Spim<hal::pac::SPIM0>, gpio::p0::P0_02<Output<PushPull>>>;
    type TimerInstance = Timer<hal::pac::TIMER0, hal::timer::Periodic>;

    // Shared resources go here
    #[shared]
    struct Shared {
        magneto: Mlx90363Instance,
        prod: Producer<'static, opcode::Response, 2>,
        cons: Consumer<'static, opcode::Response, 2>,
    }

    // Local resources go here
    #[local]
    struct Local {
        timer: TimerInstance,
    }

    #[init(local = [q: Queue<opcode::Response, 2> = Queue::new()])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let (p, c) = cx.local.q.split();

        let dp = cx.device;
        let _cp = cx.core;

        let p0 = gpio::p0::Parts::new(dp.P0);
        let p1 = gpio::p1::Parts::new(dp.P1);

        let spi_clk = p0.p0_08.into_push_pull_output(gpio::Level::Low).degrade();
        let spi_mosi = p1.p1_09.into_push_pull_output(gpio::Level::Low).degrade();
        let spi_miso = p0.p0_13.into_floating_input().degrade();
        let spi_cs = p0.p0_02.into_push_pull_output(gpio::Level::High);
        let spi = Spim::new(
            dp.SPIM0,
            spim::Pins {
                sck: spi_clk,
                miso: Some(spi_miso),
                mosi: Some(spi_mosi),
            },
            spim::Frequency::K125,
            spim::MODE_1,
            0,
        );

        let mlx90363 = Mlx90363::new(spi, spi_cs).unwrap();

        let mut timer0 = Timer::periodic(dp.TIMER0);
        timer0.enable_interrupt();
        timer0.start(2_000_000u32);

        defmt::info!("init");

        // tStartUp > 23.2 ms
        startup::spawn_after(30.millis()).ok();

        (
            Shared {
                magneto: mlx90363,
                prod: p,
                cons: c,
            },
            Local { timer: timer0 },
            init::Monotonics(MonoTimer::new(dp.TIMER1)),
        )
    }

    // Optional idle, can be removed if not needed.
    #[idle]
    fn idle(_: idle::Context) -> ! {
        defmt::info!("idle");

        loop {
            continue;
        }
    }

    // Startup task to send the first message (should be a NOP) before the *main loop*.
    #[task(shared = [magneto])]
    fn startup(mut cx: startup::Context) {
        defmt::info!("Hello from task1!");
        cx.shared.magneto.lock(|m| m.nop(0x1234).ok());
        regular::spawn_after(5.millis()).ok();
    }

    // Main loop for getting regular messages (containing positioning data)
    // The data is added to a queue to let another task handle it.
    #[task(shared = [magneto, prod])]
    fn regular(mut cx: regular::Context) {
        cx.shared.magneto.lock(|m| {
            if let Ok(res) = m.get1(opcode::Marker::Alpha, 10000, false) {
                cx.shared.prod.lock(|p| {
                    let _ = p.enqueue(res);
                });
            }
        });
        regular::spawn_after(5.millis()).ok();
    }

    // This tasks handles the responses from the magnetic positioning sensor.
    #[task(binds = TIMER0, local = [timer], shared = [cons])]
    fn task2(mut cx: task2::Context) {
        // reset timer
        cx.local.timer.event_compare_cc0().write(|w| w);
        cx.shared.cons.lock(|c| {
            while let Some(item) = c.dequeue() {
                defmt::info!("{:?}", defmt::Debug2Format(&item));
            }
        });
    }
}
